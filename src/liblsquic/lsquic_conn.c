/* Copyright (c) 2017 - 2020 LiteSpeed Technologies Inc.  See LICENSE. */
#include <assert.h>
#include <inttypes.h>
#include <string.h>
#include <sys/queue.h>

#include <openssl/rand.h>

#include "lsquic.h"
#include "lsquic_int_types.h"
#include "lsquic_hash.h"
#include "lsquic_conn.h"
#include "lsquic_packet_common.h"
#include "lsquic_packet_gquic.h"
#include "lsquic_packet_in.h"
#include "lsquic_str.h"
#include "lsquic_enc_sess.h"
#include "lsquic_mm.h"
#include "lsquic_engine_public.h"
#include "lsquic_ev_log.h"

#include "lsquic_logger.h"


#include <stdio.h>
#include <stdlib.h>
int lsquic_random_cid = 0;

const lsquic_cid_t *
lsquic_conn_id(const lsquic_conn_t *lconn) {
    /* TODO */
    return lsquic_conn_log_cid(lconn);
}


void *
lsquic_conn_get_peer_ctx (struct lsquic_conn *lconn,
                                            const struct sockaddr *local_sa)
{
    const struct network_path *path;

    path = lconn->cn_if->ci_get_path(lconn, local_sa);
    return path->np_peer_ctx;
}


unsigned char
lsquic_conn_record_sockaddr (lsquic_conn_t *lconn, void *peer_ctx,
            const struct sockaddr *local_sa, const struct sockaddr *peer_sa)
{
    return lconn->cn_if->ci_record_addrs(lconn, peer_ctx, local_sa, peer_sa);
}


int
lsquic_conn_get_sockaddr (struct lsquic_conn *lconn,
                const struct sockaddr **local, const struct sockaddr **peer)
{
    const struct network_path *path;

    path = lconn->cn_if->ci_get_path(lconn, NULL);
    *local = NP_LOCAL_SA(path);
    *peer = NP_PEER_SA(path);
    return 0;
}


int
lsquic_conn_copy_and_release_pi_data (const lsquic_conn_t *conn,
          struct lsquic_engine_public *enpub, lsquic_packet_in_t *packet_in)
{
    assert(!(packet_in->pi_flags & PI_OWN_DATA));
    /* The size should be guarded in lsquic_engine_packet_in(): */
    assert(packet_in->pi_data_sz <= GQUIC_MAX_PACKET_SZ);
    unsigned char *const copy = lsquic_mm_get_packet_in_buf(&enpub->enp_mm, 1370);
    if (!copy)
    {
        LSQ_WARN("cannot allocate memory to copy incoming packet data");
        return -1;
    }
    memcpy(copy, packet_in->pi_data, packet_in->pi_data_sz);
    packet_in->pi_data = copy;
    packet_in->pi_flags |= PI_OWN_DATA;
    return 0;
}


enum lsquic_version
lsquic_conn_quic_version (const lsquic_conn_t *lconn)
{
    if (lconn->cn_flags & LSCONN_VER_SET)
        return lconn->cn_version;
    else
        return -1;
}


enum lsquic_crypto_ver
lsquic_conn_crypto_ver (const lsquic_conn_t *lconn)
{
    return LSQ_CRY_QUIC;
}


const char *
lsquic_conn_crypto_cipher (const lsquic_conn_t *lconn)
{
    if (lconn->cn_enc_session)
        return lconn->cn_esf_c->esf_cipher(lconn->cn_enc_session);
    else
        return NULL;
}


int
lsquic_conn_crypto_keysize (const lsquic_conn_t *lconn)
{
    if (lconn->cn_enc_session)
        return lconn->cn_esf_c->esf_keysize(lconn->cn_enc_session);
    else
        return -1;
}


int
lsquic_conn_crypto_alg_keysize (const lsquic_conn_t *lconn)
{
    if (lconn->cn_enc_session)
        return lconn->cn_esf_c->esf_alg_keysize(lconn->cn_enc_session);
    else
        return -1;
}


struct stack_st_X509 *
lsquic_conn_get_server_cert_chain (struct lsquic_conn *lconn)
{
    if (lconn->cn_enc_session)
        return lconn->cn_esf_c->esf_get_server_cert_chain(lconn->cn_enc_session);
    else
        return NULL;
}


void
lsquic_conn_make_stream (struct lsquic_conn *lconn)
{
    lconn->cn_if->ci_make_stream(lconn);
}


unsigned
lsquic_conn_n_pending_streams (const struct lsquic_conn *lconn)
{
    return lconn->cn_if->ci_n_pending_streams(lconn);
}


unsigned
lsquic_conn_n_avail_streams (const struct lsquic_conn *lconn)
{
    return lconn->cn_if->ci_n_avail_streams(lconn);
}


unsigned
lsquic_conn_cancel_pending_streams (struct lsquic_conn *lconn, unsigned count)
{
    return lconn->cn_if->ci_cancel_pending_streams(lconn, count);
}


void
lsquic_conn_going_away (struct lsquic_conn *lconn)
{
    lconn->cn_if->ci_going_away(lconn);
}


void
lsquic_conn_close (struct lsquic_conn *lconn)
{
    lconn->cn_if->ci_close(lconn);
}


int
lsquic_conn_is_push_enabled (lsquic_conn_t *lconn)
{
    return lconn->cn_if->ci_is_push_enabled(lconn);
}


struct lsquic_engine *
lsquic_conn_get_engine (struct lsquic_conn *lconn)
{
    return lconn->cn_if->ci_get_engine(lconn);
}


int
lsquic_conn_push_stream (struct lsquic_conn *lconn, void *hset,
    struct lsquic_stream *stream, const struct lsquic_http_headers *headers)
{
    return lconn->cn_if->ci_push_stream(lconn, hset, stream, headers);
}


lsquic_conn_ctx_t *
lsquic_conn_get_ctx (const struct lsquic_conn *lconn)
{
    return lconn->cn_if->ci_get_ctx(lconn);
}


void
lsquic_conn_set_ctx (struct lsquic_conn *lconn, lsquic_conn_ctx_t *ctx)
{
    lconn->cn_if->ci_set_ctx(lconn, ctx);
}


void
lsquic_conn_abort (struct lsquic_conn *lconn)
{
    lconn->cn_if->ci_abort(lconn);
}

/* This function is called twice initially to set
 * the source and destination connection ids.
 * The third time on, it's for the
 * NEW_CONNECTION_ID_FRAME with new cids.
 * I've modified this in a quick and dirty way
 * to toggle between permutations of the cids
 * that are set for the INITIAL packets and
 * for those after, i.e., when one peer wants
 * to share new cids with its remote peer.
 * TODO What's currently missing is the ability to finely control how the new cids are going to be, i.e., also random or deterministic. */
void
lsquic_generate_cid (lsquic_cid_t *cid, size_t len) {
    if (lsquic_random_cid > 1) {

        //Todo have a nice toggle for the random cid
        /* Code for random cid  */
        if (!len)
        {
            /* If not set, generate ID between 8 and MAX_CID_LEN bytes in length */
            RAND_bytes((uint8_t *) &len, sizeof(len));
            len %= MAX_CID_LEN - 7;
            len += 8;
        }
        RAND_bytes(cid->idbuf, len);
        cid->len = len;

//       if (!len) {
//           /* If not set, generate ID for Destination and set len to 8 */
//           *cid->idbuf = 3;
//           cid->len = 8;
//       } else {
//           /* generate ID for Source */
//           *cid->idbuf = 3;
//           cid->len = 8;
//       }
    } else {
        const char* s = getenv("LSQUIC_CID");
        if (s!=NULL) {
            char  tmpStrings[20][20];
            int wordctr, i, j = 0;
            for (i=0; s[i] != '\0' && wordctr < 20; i++) {
                if (s[i] == ' ' || s[i] == '\0') {
                    tmpStrings[wordctr][j] = '\0';
                    wordctr++;
                    j=0;
                } else {
                    tmpStrings[wordctr][j] = s[i];
                    j++;
                }
            }
            for(i = 0; i <= wordctr; i++) {
                cid->idbuf[i] = (uint8_t)atoi(tmpStrings[i]);
            }
            cid->len= wordctr < 8?8:wordctr;
            
        }
        else if (!len) {
            /* If not set, generate ID for Destination and set len to 8 */
            *cid->idbuf=2;
            cid->len = 8;
        } else {
            /* generate ID for Source */
            *cid->idbuf = 1;
            cid->len = 8;
        }
        lsquic_random_cid++;
    }
}


void
lsquic_generate_cid_gquic (lsquic_cid_t *cid)
{
    lsquic_generate_cid(cid, GQUIC_CID_LEN);
}


void
lsquic_conn_retire_cid (struct lsquic_conn *lconn)
{
    if (lconn->cn_if->ci_retire_cid)
        lconn->cn_if->ci_retire_cid(lconn);
}


enum LSQUIC_CONN_STATUS
lsquic_conn_status (struct lsquic_conn *lconn, char *errbuf, size_t bufsz)
{
    return lconn->cn_if->ci_status(lconn, errbuf, bufsz);
}


const lsquic_cid_t *
lsquic_conn_log_cid (const struct lsquic_conn *lconn)
{
    if (lconn->cn_if && lconn->cn_if->ci_get_log_cid)
        return lconn->cn_if->ci_get_log_cid(lconn);
    return CN_SCID(lconn);
}
